<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Registered</title>
</head>
<body>
<s:if test="hasActionMessages()">
<h2><s:actionmessage/></h2>
<s:a value="/Login.jsp">To Login Click Here:</s:a>
</s:if>
</body>