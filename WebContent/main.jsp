<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">

<script src="Scripts/bootstrap2.min.js"></script>
<script src="Scripts/jquery.min.js"></script>
<script src="Scripts/bootstrap.min.js"></script>

<title>HOME</title>
<style>
.jumbotron {
	background-position: center;
	background-size: cover;
	background-image: url("images/HomeImage.jpg");
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<div class="navbar-collapse collapse show" id="navbarColor01" style="">

		<s:url id="localeEN" namespace="/" action="locale">
			<s:param name="request_locale">en</s:param>
		</s:url>
		<s:url id="localeHN" namespace="/" action="locale">
			<s:param name="request_locale">hn</s:param>
		</s:url>
		<ul class="navbar-nav mr-auto">
			<s:a href="%{localeEN}" class="nav-link">English</s:a>
			<s:a href="%{localeHN}" class="nav-link">Hindi</s:a>
		</ul>
		<ul class="navbar-nav mr-auto">

		</ul>


		<ul class="nav navbar-nav pull-right">
			<a href="Login.jsp" class="nav-link">Login</a>
		</ul>
		<ul class="nav navbar-nav pull-right">
			<a href="UserRegistration.jsp" class="nav-link">Sign Up</a>
		</ul>
		</ul>
	</div>
	</nav>

	<div class="jumbotron container-fluid ">
		<br> <br>
		<h1 class="display-1 text-center" style="color: white">Knowledge
			Router</h1>
		<p class="display-4 text-center" style="color: white">Let's make a
			difference between survival and existence!!!!</p>
		<br> <br> <br>
	</div>
	<div class="container">
		<h1 class="display-5 text-center">About The ROUTER</h1>
		<br> <br>
		<p class="lead text-center">A platform where the dignitary(the
			responsible person for a particular area or place) can register their
			requirements for a tutor in remote areas, non governmental
			organizations, orphanages, etc where awareness and spreading of
			education is needed. And on the same platform interested folks will
			register themselves and as requirements come in as per their
			availability and time period they pick the respective options/queries
			and contact the concerned person directly.</p>
		<p class="lead text-center">Also this forum will provide a option
			to donate books, clothes for needy people and anyone will be free to
			donate as per their wish , free of cost no laurels provided</p>

	</div>
</body>
</html>