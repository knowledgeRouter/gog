<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w4.css">
<script src="Scripts/jquery.min.js"></script>
<script src="Scripts/bootstrap2.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css" id="bootstrap.css">
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/jquery.min1.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Donar List</title>
</head>
<body  class="Container">
<div>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<div class="navbar-collapse collapse show" id="navbarColor01" style="">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"><a href="normalUser.action"
				class="nav-link">Back To Profile</a></li>
		</ul>
		<ul class="nav navbar-nav pull-right">
			<div class=" dropdown">
				<a href="#" class="dropdown-toggle active nav-link"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false"><s:label name="#session.username" /><span
					class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><s:a value="logout.action"
							class="w3-button w3-block w3-black">Logout</s:a></li>
				</ul>
			</div>
		</ul>
	</div>
	</nav>
	</br>
	<h4>Donor Details</h4>
	<table class="table table-hover">
		<tbody>
			<tr class="table-danger" style="width: 100px">
				<th scope="row">Donar Name</th>
				<!-- <th scope="row">Donar Email</th>
				<th scope="row">Donar Phone</th> -->
				<th scope="row">Item</th>
				<th scope="row">Quantity</th>
				<th scope="row">Description</th>
				<th scope="row">PickUpLocation</th>
				<th scope="row">Notify</th>
			</tr>
			<s:set var="flag" value="%{flag}"/>
			<s:iterator value="donateList">
				<tr>
					<td><s:property value="%{firstName+lastName}" /></td>
					<%-- <td><s:property value="%{email}" /></td>
					<td><s:property value="%{phone}" /></td> --%>
					<td><s:property value="%{item}" /></td>
					<td><s:property value="%{quantity}" /></td>
					<td><s:property value="%{description}" /></td>
						<td><s:property value="%{address}" /></td>
						<s:text var="enquire" name="Send Email to: %{email}">
						</s:text>
						<s:text var="unableToEnquire"
							name="Please complete your profile first">
						</s:text>
						<td><s:if test="#flag == 0">
								<s:a
									href="sendNotificaion.action?nUId=%{nUId}&sendTo=%{email}&sendToUser=%{userId}">Interest</s:a>
							</s:if> <s:if test="#flag == 1">
								<s:a href="normalUser.action" title="%{#unableToEnquire}">Unable to Enquire</s:a>
							</s:if></td>
					</tr>
			</s:iterator>
		</tbody>
	</table>
	<s:if test="hasActionErrors()">
			<h3><s:actionerror /></h3>
		</s:if>

		<s:if test="hasActionMessages()">
			<h3><s:actionmessage /></h3>
		</s:if>
		</div>
</body>
</html>