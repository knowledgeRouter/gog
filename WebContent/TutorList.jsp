<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="Scripts/jquery.min.js"></script>
<script src="Scripts/bootstrap2.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css" id="bootstrap.css">
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/jquery.min1.js"></script>
<title>Tutor List</title>
<style>
</style>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<div class="navbar-collapse collapse show" id="navbarColor01" style="">
		<ul class="navbar-nav mr-auto">

			<li class="nav-item"><a href="normalUser.action"
				class="nav-link">Back To Profile</a></li>

		</ul>
		<ul class="nav navbar-nav pull-right">
			<div class=" dropdown">
				<a href="#" class="dropdown-toggle active nav-link"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false"><s:label name="#session.username" /><span
					class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><s:a value="logout.action"
							class="w3-button w3-block w3-black">Logout</s:a></li>
				</ul>
			</div>
		</ul>
		</ul>
	</div>
	</nav>
	<div>
		<br>
		<h4>Tutor Details</h4>

		<table class="table table-hover">
			<tbody>
				<tr class="table-danger" style="width: 100px">
					<th scope="row">Tutor Name</th>
					<!-- <th scope="row">Tutor Email</th>
					<th scope="row">Tutor Phone</th> -->
					<th scope="row">Tutor Interest</th>
					<th scope="row">Tutor Preferred Location</th>
					
					<th scope="row">Enquire</th>
				</tr>
				<s:set var="flag" value="%{flag}"/>
				<s:iterator value="tutorList">
					<tr>
						<td><s:property value="%{firstName}" /></td>
						<%-- <td><s:property value="%{email}" /></td>
						<td><s:property value="%{phone}" /></td> --%>
						<td><s:property value="%{interest}" /></td>
						<td><s:property value="%{area}" /></td>
						
						<s:text var="enquire" name="Send Email to: %{email}">
							
						</s:text>
						<s:text var="unableToEnquire" name="Please complete your profile first">
							
						</s:text>
						<td> <s:if test="#flag == 0"> 
								<s:a
									href="sendNotificaion.action?nUId=%{nUId}&sendTo=%{email}&sendToUser=%{userId}"
									title="%{#enquire}">Enquire</s:a>
							 </s:if> 
							 <s:if test="#flag == 1"> 
							 	<s:a
									href="normalUser.action"
									title="%{#unableToEnquire}" >Unable to Enquire</s:a>
							</s:if> </td>
					</tr>
					
				</s:iterator>
			</tbody>
		</table>
		<s:if test="hasActionErrors()">
			<s:actionerror />
		</s:if>
		<s:if test="hasActionMessages()">
			<s:actionmessage />
		</s:if>
	</div>

</body>
</html>