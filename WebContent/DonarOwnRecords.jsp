<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w4.css">
<script src="Scripts/jquery.min.js"></script>
<script src="Scripts/bootstrap2.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css" id="bootstrap.css">
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/jquery.min1.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tutor List</title>
</head>
<body class="Container">
	<div>
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<div class="navbar-collapse collapse show" id="navbarColor01" style="">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a href="donate.action" class="nav-link">Back
						To Profile</a></li>
			</ul>
			<ul class="nav navbar-nav pull-right">
				<div class=" dropdown">
					<a href="#" class="dropdown-toggle active nav-link"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"><s:label name="#session.username" /><span
						class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><s:a value="logout.action"
								class="w3-button w3-block w3-black">Logout</s:a></li>
					</ul>
				</div>
			</ul>
			</ul>
		</div>
		</nav>

		<br>
		<h4>Donations done</h4>
		<table class="table table-hover">
			<tbody>
				<tr class="table-danger" style="width: 100px">
					<th scope="row">Item</th>
					<th scope="row">Quantity</th>
					<th scope="row">Description</th>
				</tr>
				<s:iterator value="donateOwnList">
					<tr>

						<td><s:property value="%{item}" /></td>
						<td><s:property value="%{quantity}" /></td>
						<td><s:property value="%{description}" /></td>
					</tr>
				</s:iterator>
			</tbody>

		</table>
	</div>
</body>
</html>