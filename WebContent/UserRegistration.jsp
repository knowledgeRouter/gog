<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/w10.css">
<link rel="stylesheet" href="css/w3.css">
<script src="Scripts/bootstrap2.min.js"></script>
<script src="Scripts/jquery.min.js"></script>
<script src="Scripts/bootstrap.min.js"></script>


<title>UserRegistration</title>
<style>
input[type=text], select {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
}

input[type=submit] {
	width: 100%;
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	border-radius: 4px;
	cursor: pointer;
}

input[type=submit]:hover {
	background-color: #45a049;
}

body, html {
	height: 100%;
	width: 100%;
	font-family: "Inconsolata", sans-serif;
	background-color: white; */
	margin: 0px auto;
	background-image: url("images/signupimage.jpg");
	background-size: cover;
	background-position: top center !important;
	background-repeat: no-repeat !important;
	background-attachment: fixed;
}

label[for="firstname"] {
	color: white;
	font-family: "Inconsolata", sans-serif;
	font-weight: bold;
}
</style>

</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<div class="navbar-collapse collapse show" id="navbarColor01" style="">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"><a href="main.jsp" class="nav-link">Home</a>
			</li>
		</ul>
		</ul>
	</div>
	</nav>

	<div class="container">
		<div id="signup">
			<div class="header">

				<h3 style="font-size: 40px; color: white;">SignUp</h3>

				<h6 style="font-size: 20px; color: white;">You need to fill out
					this form</h6>

			</div>
			<s:form action="userRegistration">
				<s:textfield cssStyle="width:300px;height:30px" id="firstname"
					label="First Name:" key="firstName" />
				<s:textfield cssStyle="width:300px;height:30px " id="firstname"
					label="Last Name:" key="lastName" />
				<s:textfield cssStyle="width:300px;height:30px " label="UserName:"
					key="userName" id="firstname" />
				<s:textfield cssStyle="width:300px;height:30px " id="firstname"
					label="Email Id:" key="email" />
				<s:password label="Password:" id="firstname" key="password" />
				<s:textfield cssStyle="width:300px;height:30px " id="firstname"
					label="Mobile Number:" key="phone" />
				<s:select cssStyle="width:300px;height:50px " id="firstname"
					label="Type of User: "
					list="#{'0':'Beneficiary','1':'Tutor','2':'Donate'}" key="type"
					headerKey="-1" headerValue="Select User"></s:select>
				<s:submit cssClass="button1 w3-black" label="Sign Up" />
			</s:form>
			<s:if test="hasActionMessages()">
				<h2>
					<s:actionmessage />
				</h2>
			</s:if>
			<s:if test="hasActionErrors()">
				<h2>
					<s:actionerror />
				</h2>
			</s:if>
		</div>
	</div>
</body>
</html>
