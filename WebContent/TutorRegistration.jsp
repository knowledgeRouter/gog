<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<link rel="stylesheet" href="css/w10.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="Scripts/bootstrap2.min.js"></script>
<script src="Scripts/jquery.min.js"></script>
<script src="Scripts/jquery.min1.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css" id="bootstrap.css">
<script src="Scripts/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<title>TutorProfile</title>
<style>
input[type=text], select {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
}

input[type=submit] {
	width: 100%;
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	border-radius: 4px;
	cursor: pointer;
}

input[type=submit]:hover {
	background-color: #45a049;
}

.w3-border {
	border: 3px solid !important;
	border-color: #3f51b5 !important;
}

body, html {
	height: 100%;
	width: 100%;
	font-family: "Inconsolata", sans-serif;
	background-color: white;
	background-image: url("images/profileimage1.jpg");
	background-size: 50%;
	background-position: right !important;
	background-repeat: no-repeat !important;
	background-attachment: inherit;
}

label[for="style"] {
	color: black;
	font-size: 16px;
	font-weight: bold;
	letter-spacing: 1px;
	margin-bottom: 7px;
	display: block;
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<div class="navbar-collapse collapse show" id="navbarColor01" style="">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"><s:a
					href="fetchNormalUserRecords.action?nUId=%{userId}"
					class="nav-link">Beneficiary Avaliable</s:a></li>

		</ul>
		<ul class="nav navbar-nav pull-right">
			<div class=" dropdown">
				<a href="#" class="dropdown-toggle active nav-link"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false"><s:label name="#session.username" /><span
					class="caret"></span></a>

				<ul class="dropdown-menu">
					<li><s:a value="logout.action"
							class="w3-button w3-block w3-black">Logout</s:a></li>
				</ul>
			</div>
		</ul>
		
	</div>
	</nav>
	<div class="container">
		<div id="signup">
			<div class="header">

				<h3 style="font-size: 40px; color: black;">PROFILE UPDATE</h3>

				<h6 style="font-size: 20px; color: black;">Please provide us
					with your complete details</h6>

			</div>



			<!-- <div class="inputs">  -->
			<s:form action="saveTutorRegDetails">
				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="First Name:" value="%{firstName}" readonly="true"
					disabled="true" />
				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="Last Name:" value="%{lastName}" readonly="true"
					disabled="true" />
				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="Email Address:" value="%{email}" readonly="true"
					disabled="true" />
				<s:textarea cssStyle="width:250px;height:30px " id="style"
					label="Address:" value="%{address}" key="address" 
					/>
				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="Mobile Number:" value="%{phone}" readonly="true"
					disabled="true" />
				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="Subject of Expertise:" key="interest" class="w3-border" />
				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="Preferred Location:" key="area" class="w3-border" />
				<s:hidden label="UserId:" value="%{userId}" key="userId"
					readonly="true" />
				<s:submit value="Save Details" class="w3-button w3-green" />
				<s:if test="hasActionMessages()">
				<h2>
					<s:actionmessage />
					</h2>
				</s:if>
				<s:if test="hasActionErrors()">
					<h2>
						<s:actionmessage />
					</h2>
				</s:if>

			</s:form>
		</div>
	</div>





</body>
</html>