<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="css/w10.css">
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/bootstrap2.min.js"></script>
<script src="Scripts/jquery.min.js"></script>
<script src="Scripts/jquery.min1.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css" id="bootstrap.css">
<script src="Scripts/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Inconsolata">
<title>Donate</title>
<style>
body, html {
	height: 100%;
	width: 100%;
	font-family: "Inconsolata", sans-serif;
	background-image: url("images/profileimage1.jpg");
	-moz-background-size: cover;
	-webkit-background-size: cover;
	background-size: 50%;
	background-position: right !important;
	background-repeat: no-repeat !important;
	background-attachment: inherit;
}

.container {
	padding-top: 70px;
	width: 406px;
	max-width: 406px;
	margin: 0 auto;
}
input[type=submit] {
	width: 100%;
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	border-radius: 4px;
	cursor: pointer;
}

input[type=submit]:hover {
	background-color: #45a049;
}

label[for="style"] {
	color: black;
	font-size: 16px;
	font-weight: bold;
	letter-spacing: 1px;
	margin-bottom: 7px;
	display: block;
}

.w3-border {
	border: 3px solid !important;
	border-color: #3f51b5 !important;
}
</style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<div class="navbar-collapse collapse show" id="navbarColor01" style="">
		<ul class="navbar-nav mr-auto">
		<li class="nav-item"><s:a href="fetchOwnDonateRecords.action" class="nav-link">Donations Done</s:a></li>
		</ul>
		<ul class="nav navbar-nav pull-right">
			<div class=" dropdown">
				<a href="#" class="dropdown-toggle active nav-link"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false"><s:label name="#session.username" /><span
					class="caret"></span></a>

				<ul class="dropdown-menu">
					<li><s:a value="logout.action">Logout</s:a></li>
				</ul>
			</div>
		</ul>
		</ul>
	</div>
	</nav>
<div class="container-fluid">

		<div id="signup">
			<div class="header">

				<h3 style="font-size: 40px; color: black;">DONATION FORM</h3>

				<h6 style="font-size: 20px; color: black;;">Please make a
					little Contribution to the society!</h6>

			</div>




			<s:form action="saveDonateUserRegDetails">


				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="First Name" key="firstName" disabled="true"/>

				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="Mobile Number" key="phone" disabled="true"/>
				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="Email ID" key="email" disabled="true"/>
				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="Item" key="item" class="w3-border" />
				<s:textarea cssStyle="width:250px;height:30px " id="style"
					label="Description of item" key="description" cssClass="w3-border" />
				<s:textfield cssStyle="width:250px;height:30px " id="style"
					label="Quantity" key="quantity" class="w3-border" />

				<s:textarea cssStyle="width:250px;height:30px " id="style"
					label="PickUp Location" cssClass="w3-border"
					key="address" />
				<s:hidden label="UserId:" value="%{userId}" key="userId"
					readonly="true" />
				<s:submit value="Make A Donation" class="w3-button w3-black" />

			

			<s:if test="hasActionMessages()">
				<h2>
					<s:actionmessage />
				</h2>
			</s:if>
			<s:if test="hasActionErrors()">
				<h2>
					<s:actionerror />
				</h2>
			</s:if>
</s:form>

			


		</div>

	</div>
	</div>
	​
	</div>
</body>
</html>