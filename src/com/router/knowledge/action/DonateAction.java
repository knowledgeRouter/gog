package com.router.knowledge.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.ActionSupport;
import com.router.knowldege.beans.DonarBean;

public class DonateAction extends ActionSupport implements SessionAware{
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String item;
	private String description;
	private int quantity;
	private String address;
	private Map<String,Object> session;
	private String userId;
	private String nUId;
	private ArrayList<DonarBean> donateList=new ArrayList<DonarBean>();
	private ArrayList<DonarBean> donateOwnList=new ArrayList<DonarBean>();
	private int flag =0;

	
	
	public int getFlag() {
		return flag;
	}


	public void setFlag(int flag) {
		this.flag = flag;
	}


	public ArrayList<DonarBean> getDonateOwnList() {
		return donateOwnList;
	}


	public void setDonateOwnList(ArrayList<DonarBean> donateOwnList) {
		this.donateOwnList = donateOwnList;
	}


	public String getnUId() {
		return nUId;
	}


	public void setnUId(String nUId) {
		this.nUId = nUId;
	}


	public ArrayList<DonarBean> getDonateList() {
		return donateList;
	}


	public void setDonateList(ArrayList<DonarBean> donateList) {
		this.donateList = donateList;
	}


	public Map<String, Object> getSession() {
		return session;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getItem() {
		return item;
	}


	public void setItem(String item) {
		this.item = item;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}

@Override
public void validate() {
	if(StringUtils.isBlank(getItem()))
	{
		addFieldError("item", "Item cannot be blank");
	}
	if(StringUtils.isBlank(getDescription()))
	{
		addFieldError("description", "Description cannot be blank");
	}
	if(StringUtils.isBlank(getAddress()))
	{
		addFieldError("address", "Address cannot be blank");
	}
	if(getQuantity()==0 || !StringUtils.isNumeric(String.valueOf(getQuantity())))
	{
		addFieldError("quantity", "Invalid quantity");
	}
}
	
	@SkipValidation
	public String execute()
	{
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql = "SELECT FirstName,LastName,EmailId,ContactNo,UserId from personalinformation where userName=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, (String)session.get("username"));
			System.out.println("user email:"+session.get("username"));
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				/*String sql1="SELECT Item,DescriptionOfItem,Quantity,AddressWhereToPick from donate where UserId=?";
				PreparedStatement ps1=connection.prepareStatement(sql1);*/
				
				setFirstName(rs.getString(1));
				setLastName(rs.getString(2));
				setEmail(rs.getString(3));
				setPhone(rs.getString(4));
				setUserId(rs.getString(5));
				System.out.println("userid:"+getUserId());
				/*ps1.setString(1, getUserId());
				ResultSet donateUserInfoSet=ps1.executeQuery();
				while(donateUserInfoSet.next())
				{
					setItem(donateUserInfoSet.getString(1));
					setDescription(donateUserInfoSet.getString(2));
					setAddress(donateUserInfoSet.getString(4));
					setQuantity(donateUserInfoSet.getInt(3));
				}
				ps1.close();*/
				ret=SUCCESS;				
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Something Went Wrong!");
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;
	}
	public String saveDetails()
	{
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			/*String sql=null;
			PreparedStatement preparedStatement=connection.prepareStatement("SELECT Item,DescriptionOfItem,Quantity,AddressWhereToPick from donate where UserId=?");
			preparedStatement.setString(1, getUserId());
			ResultSet donateUserInfoSet=preparedStatement.executeQuery();
			while(donateUserInfoSet.next())
			{
				sql="UPDATE donate set Item=?,DescriptionOfItem=?,Quantity=?,AddressWhereToPick=? where UserId=?";
				PreparedStatement preparedStatement1=connection.prepareStatement(sql);
				
				preparedStatement1.setString(1, getItem());
				preparedStatement1.setString(2, getDescription());
				preparedStatement1.setInt(3, getQuantity());
				preparedStatement1.setString(4,getAddress() );
				preparedStatement1.setString(5, getUserId());
				int rs=preparedStatement1.executeUpdate();
				if(rs==1)
				{
					System.out.println("Entry updated");
					addActionMessage("Details Updated!");
					ret=SUCCESS;
				}
			}*/
			/*if(sql==null)
			{*/
			 String sql = "INSERT INTO donate(UserId,Item,DescriptionOfItem,Quantity,AddressWhereToPick)"
					+ "VALUES(?,?,?,?,?)";
			System.out.println("User iD:"+getUserId());
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, getUserId());
			ps.setString(2, getItem());
			ps.setString(3, getDescription());
			ps.setInt(4, getQuantity());
			ps.setString(5, getAddress());
			int rs = ps.executeUpdate();

			if(rs==1)
			{
				System.out.println("Entry added");
				addActionMessage("Details Saved!");
				ret=SUCCESS;
				setItem("");
				setDescription("");
				setQuantity(0);
				setAddress("");
			}
			//}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Something Went Wrong!");
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;

	}


	@Override
	public void setSession(Map<String, Object> arg0) {
		this.session=arg0;
		
	}
	
	@SkipValidation
	public String fetchDonarRecords()
	{
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql = "SELECT UserId,Item,DescriptionOfItem,Quantity,AddressWhereToPick from donate";
			PreparedStatement ps = connection.prepareStatement(sql);
			//ps.setString(1, (String)session.get("username"));
			//System.out.println("user email:"+session.get("username"));
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				DonarBean bean=new DonarBean();
				String sql1="SELECT FirstName,LastName,EmailId,ContactNo,UserId from personalinformation where UserId=?";
				PreparedStatement ps1=connection.prepareStatement(sql1);
				ps1.setString(1, rs.getString(1));
				ResultSet donateUserInfoSet=ps1.executeQuery();
				while(donateUserInfoSet.next())
				{
					setFirstName(donateUserInfoSet.getString(1));
					setLastName(donateUserInfoSet.getString(2));
					setEmail(donateUserInfoSet.getString(3));
					setPhone(donateUserInfoSet.getString(4));
					setUserId(donateUserInfoSet.getString(5));
				}
				
				setItem(rs.getString(2));
				setDescription(rs.getString(3));
				setAddress(rs.getString(5));
				setQuantity(rs.getInt(4));				
				
				ps1.close();
				
				
				bean.setFirstName(getFirstName());
				bean.setLastName(getLastName());
				bean.setEmail(getEmail());
				bean.setPhone(getPhone());
				bean.setUserId(getUserId());
				bean.setItem(getItem());
				bean.setDescription(getDescription());
				bean.setAddress(getAddress());
				bean.setQuantity(getQuantity());
				getDonateList().add(bean);
				ret=SUCCESS;
			}
			rs.close();
			if(ret.equals(ERROR))
			{
				addActionError("No data ");
			}
			String paramName=ServletActionContext.getRequest().getParameter("nUId");
			System.out.println("Parameter name:"+paramName);
			setnUId(paramName);
			String sql2="Select UserId from personalinformation where UserId=?";
			PreparedStatement ps2=connection.prepareStatement(sql2);
			ps2.setString(1, getnUId());
			System.out.println("CHeck for:"+getnUId());
			ResultSet rsId=ps2.executeQuery();
			if(!rsId.next())
			{
				flag=1;
			}
			rsId.close();
			ps2.close();
			ret=SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Something Went Wrong!");
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;
	}
	
	@SkipValidation
	public String fetchOwnRecords()
	{
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql = "SELECT Item,DescriptionOfItem,Quantity from donate where UserId=?";
			
			PreparedStatement ps = connection.prepareStatement(sql);
			String sql1 = "SELECT UserId from personalInformation where userName=?";
			PreparedStatement ps1=connection.prepareStatement(sql1);
			ps1.setString(1, (String)session.get("username"));
			
			ResultSet rs1 = ps1.executeQuery();
			
			while(rs1.next())
			{
				ps.setString(1,rs1.getString(1));
			}
			ps1.close();
			ResultSet rs=ps.executeQuery();
			while(rs.next())
			{
				DonarBean bean=new DonarBean();
				
				
				setItem(rs.getString(1));
				setDescription(rs.getString(2));
				setQuantity(rs.getInt(3));				
				
				
				
				
				
				bean.setItem(getItem());
				bean.setDescription(getDescription());
				bean.setAddress(getAddress());
				bean.setQuantity(getQuantity());
				getDonateOwnList().add(bean);
			}
			rs.close();
			
			ret=SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Something Went Wrong!");
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;
	}

}
