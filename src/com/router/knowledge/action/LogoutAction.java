package com.router.knowledge.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

public class LogoutAction extends ActionSupport implements SessionAware{
	private Map<String,Object> session;

	@Override
	public void setSession(Map<String, Object> arg0) {
		this.session=arg0;
		
	};
	
	public String logout()
	{
		if(session.containsKey("username"))
		{
			session.remove("username");
		}
		return SUCCESS;
	}

	

}
