package com.router.knowledge.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.router.knowldege.beans.EmailUtil;

public class NotifyAction extends ActionSupport {
	private String nUId;
	private String firstName;
	private String lastName;
	private String email;
	private String address;
	private String phone;
	private String area;
	private String interest;
	private String nameOfOrg;
	private int minAge;
	private int maxAge;
	private String sendTo;
	private int userType;
	private String item;
	private String description;
	private int quantity;
	private String sendToUser;
	
	

	public String getSendToUser() {
		return sendToUser;
	}

	public void setSendToUser(String sendToUser) {
		this.sendToUser = sendToUser;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public String getnUId() {
		return nUId;
	}

	public void setnUId(String nUId) {
		this.nUId = nUId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getNameOfOrg() {
		return nameOfOrg;
	}

	public void setNameOfOrg(String nameOfOrg) {
		this.nameOfOrg = nameOfOrg;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}
	
	public String mainNotify()
	{
		
		int uid=fetchUserType();
		String sendSecondMailUid=ServletActionContext.getRequest().getParameter("sendToUser");
		System.out.println("2nd email uid:"+sendSecondMailUid);
		String emailStatus=null;
		String status=sendNotification(getnUId(),getSendTo());
		System.out.println("Send 2 nd email to :"+getEmail());
		String status1=sendNotification(sendSecondMailUid,getEmail());
		/*if(status.equals(SUCCESS) && status1.equals(SUCCESS))
		{*/
			if(uid==0)
			{
				emailStatus="normalUser";
			}
			else if(uid==1)
			{
				emailStatus="tutor";
			}
			return emailStatus;
		/*}
		else
			
			return ERROR;*/
	}

	public String sendNotification(String uid,String sendTo)
	{
		System.out.println("Interested User ID:"+uid);
		System.out.println("Send To:"+sendTo);
		String status=fetchUserData(uid);
		if(status.equals(SUCCESS))
		{
		Properties props=new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
		props.put("mail.smtp.port", "587"); //TLS Port
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		
                //create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("router.knowledge@gmail.com", "knowledge@123");
			}
		};
		Session session = Session.getInstance(props, auth);
		    
		String mailStatus=EmailUtil.sendEmail(session,sendTo, "Enquiry from Knowledge Router", createContent());
		if(mailStatus.equals(ERROR))
		{
			addActionError("Error Occurred, While Sending Email!");
			return ERROR;
		}
		else
		{
			addActionMessage("Email Sent!!");
			return SUCCESS;
			
		}
		
		}
		else
		{
			addActionError("Error Occurred, While Sending Email!");
			return ERROR;
		}
	}
	private String fetchUserData(String userId)
	{
		
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql = "SELECT FirstName,LastName,EmailId,ContactNo,UserType from personalinformation where UserId=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1,userId);

			ResultSet rs = ps.executeQuery();

			while(rs.next())
			{	
				setFirstName(rs.getString(1));
				setLastName(rs.getString(2));
				setEmail(rs.getString(3));
				setPhone(rs.getString(4));
				setUserType(rs.getInt(5));
			}
			rs.close();
			System.out.println("Get fisrt name:"+getFirstName());
			if(getUserType()==0)
			{
				System.out.println("user tyoe:"+getUserType());
			String sql1="SELECT NameOfOrg,Interest,Area,MinAgeGroup,MaxAgeGroup from normaluserinformation where UserId=?";
			PreparedStatement ps1=connection.prepareStatement(sql1);
			ps1.setString(1, userId);
			ResultSet rs1=ps1.executeQuery();
			while(rs1.next())
			{
				setNameOfOrg(rs1.getString(1));
				setInterest(rs1.getString(2));
				setArea(rs1.getString(3));
				setMinAge(rs1.getInt(4));
				setMaxAge(rs1.getInt(5));
			}
			rs1.close();
			ret=SUCCESS;
			}
			if(getUserType()==1)
			{
				String sql1="SELECT Interest,Area from tutorinformation where UserId=?";
				PreparedStatement ps1=connection.prepareStatement(sql1);
				ps1.setString(1, userId);
				ResultSet rs2=ps1.executeQuery();
				while(rs2.next())
				{
					
					setInterest(rs2.getString(1));
					setArea(rs2.getString(2));
					
				}
				rs2.close();
				ret=SUCCESS;
			}
			/*if(getUserType()==2)
			{
				String sql2="SELECT Item,DescriptionOfItem,Quantity,AddressWhereToPick from donate where UserId=?";
				PreparedStatement ps2=connection.prepareStatement(sql2);
				ps2.setString(1, userId);
				ResultSet rs2=ps2.executeQuery();
				while(rs2.next())
				{
					setItem(rs2.getString(1));
					setDescription(rs2.getString(2));
					setQuantity(rs.getInt(3));
					setAddress(rs.getString(4));
				}
				rs2.close();
				ret=SUCCESS;
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;

	}
	private String createContent()
	{
		StringBuilder sb=new StringBuilder();
		if(getUserType()==0)
		{
		
		sb.append("<html>");
		sb.append("<body>");
		sb.append("Hi,");
		sb.append("<p>"+getFirstName()+" "+ getLastName()+" of <b>"+getNameOfOrg()+"</b>"+" has shown interest in your profile.</p>");
		sb.append("<p>Please find the below details and Contact them directly!</p>");
		sb.append("<p>Name of Organization/Village:"+getNameOfOrg()+"</p>");
		if(interest!=null)
		{
			sb.append("<p>Interest In:"+getInterest()+"</p>");
		}
		sb.append("<p>Location of Organization:"+getArea()+"</p>");
		if(minAge!=0 || maxAge!=0)
		{
			sb.append("<p>Age Group:"+getMinAge()+"-"+getMaxAge()+"</p>");
		}
		sb.append("<p>Contact Info:"+getPhone()+"</p>");
		sb.append("<p>Email Id :"+getEmail()+"</p>");
		sb.append("<br/><br/>");
		sb.append("Regards,");
		sb.append("<p><b>Team Knowledge Router</b></p>");
		sb.append("</body>");
		sb.append("</html>");
		
		}
		if(getUserType()==1)
		{
			sb.append("<html>");
			sb.append("<body>");
			sb.append("Hi,");
			sb.append("<p><b>"+getFirstName()+" "+ getLastName()+"</b>  is interested in teaching and has shown interest.</p>");
			sb.append("<p>Please find the below details and Contact them directly!</p>");
			sb.append("<p>Interest In:"+getInterest()+"</p>");
			sb.append("<p>Location of Teaching:"+getArea()+"</p>");
			
			sb.append("<p>Contact Info:"+getPhone()+"</p>");
			sb.append("<p>Email Id :"+getEmail()+"</p>");
			sb.append("<br/><br/>");
			sb.append("Regards,");
			sb.append("<p><b>Team Knowledge Router</b></p>");
			sb.append("</body>");
			sb.append("</html>");
			
		}
		/*if(getUserType()==2)
		{
			sb.append("<html>");
			sb.append("<body>");
			sb.append("Hi,");
			sb.append("<p>"+getFirstName()+" "+ getLastName()+" of <b>"+getNameOfOrg()+"</b>"+" has shown interest in your donation.</p>");
			sb.append("<p>Please find the below details and Contact them directly!</p>");
			sb.append("<p>Name of Organization/Village:"+getNameOfOrg()+"</p>");
			sb.append("<p>Interest In:"+getInterest()+"</p>");
			sb.append("<p>Location of Teaching:"+getArea()+"</p>");
			sb.append("<p>Age Group:"+getMinAge()+"-"+getMaxAge()+"</p>");
			sb.append("<p>Contact Info:"+getPhone()+"</p>");
			sb.append("<p>Email Id:"+getEmail()+"</p>");
			sb.append("<br/><br/>");
			sb.append("Regards,");
			sb.append("<p><b>Team Knowledge Router</b></p>");
			sb.append("</body>");
			sb.append("</html>");
		}*/
		setFirstName("");
		setLastName("");
		setAddress("");
		setPhone("");
		setInterest("");
		setArea("");
		setNameOfOrg("");
		setItem("");
		setDescription("");
		setQuantity(0);
		setMinAge(0);
		setMaxAge(0);
		return sb.toString();
	}
	private int fetchUserType()
	{
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql = "SELECT UserType from personalinformation where UserId=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1,getnUId());

			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				userType=rs.getInt(1);
			}
			rs.close();
			ps.close();
			return userType;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return -1;
		}
	}

}
