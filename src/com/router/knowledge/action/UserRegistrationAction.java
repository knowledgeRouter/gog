package com.router.knowledge.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import org.apache.commons.lang3.StringUtils;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.IntRangeFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.sun.glass.ui.GestureSupport;

public class UserRegistrationAction extends ActionSupport{
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	//private String address;
	private String phone;
	private int type; 
	private String userName;
	

	public String getUserName() {
		return userName;
	}

	@RequiredStringValidator(message = "Please enter userName")
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}
	
	@RequiredStringValidator(message = "Please enter fisrtName")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	@RequiredStringValidator(message = "Please enter lastName")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}
	
	@RequiredStringValidator(message = "Please enter email id")
	@EmailValidator(message = "Please enter a valid email id")
	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	@RequiredStringValidator(message = "Please enter password")
	public void setPassword(String password) {
		this.password = password;
	}

	

	public String getPhone() {
		return phone;
	}

	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

@Override
public void validate() {
	if(!StringUtils.isNumeric(getPhone()))
	{
		addFieldError("phone", "Enter valid phoneNumber");
	}
}

	@Override
	public String execute() throws Exception {
		String ret = ERROR;
		Connection connection=null;
		 try {
	         String URL = "jdbc:mysql://localhost/knowledge_router";
	         Class.forName("com.mysql.jdbc.Driver");
	         connection = DriverManager.getConnection(URL, "root", "root");
	         System.out.println("Phone no:"+getPhone());
	         String sql = "INSERT INTO personalinformation(FirstName ,LastName ,EmailId ,Password ,ContactNo,UserType,userName) VALUES(?,?,?,?,?,?,?)";
	      //   sql+=" user = ? AND password = ?";
	         PreparedStatement ps = connection.prepareStatement(sql);
	       //  ps.setInt(1,1);
	         ps.setString(1, getFirstName());
	         ps.setString(2, getLastName());
	         ps.setString(3, getEmail());
	         ps.setString(4, getPassword());
	         ps.setString(5, getPhone());
	         ps.setInt(6, getType());
	         ps.setString(7, getUserName());
	         int rs = ps.executeUpdate();

	         if(rs==1) {
	        	addActionMessage("User Created Successfully!!");
	            ret = SUCCESS;
	         }
	      } 
		 catch(MySQLIntegrityConstraintViolationException sqlExc)
		 {
			 addActionError("Please provide a unique username");
		 }
		 catch (Exception e) {
	    	  e.printStackTrace();
	    	  addActionError("Something Went Wrong");
	         ret = ERROR;
	      } finally {
	         if (connection != null) {
	            try {
	            	connection.close();
	            } catch (Exception e) {
	            }
	         }
	      }
	      return ret;
		
	}

}
