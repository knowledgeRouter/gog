package com.router.knowledge.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.ActionSupport;
import com.router.knowldege.beans.NormalUserBean;
import com.router.knowldege.beans.TutorBean;

public class NormalUserAction extends ActionSupport implements SessionAware{
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String address;
	private String phone;
	private String area;
	private String interest;
	private String nameOfOrg;
	private int minAge;
	private int maxAge;
	private String userId;
	private String nUId;
	private ArrayList<NormalUserBean> normalUserList;
	private Map<String,Object> session;
	private int flag=0;

	public ArrayList<NormalUserBean> getNormalUserList() {
		if(normalUserList==null){
			normalUserList=new ArrayList<NormalUserBean>();
		}
		return normalUserList;
	}


	public void setNormalUserList(ArrayList<NormalUserBean> normalUserList) {
		this.normalUserList = normalUserList;
	}




	public int getFlag() {
		return flag;
	}


	public void setFlag(int flag) {
		this.flag = flag;
	}


	public String getnUId() {
		return nUId;
	}


	public void setnUId(String nUId) {
		this.nUId = nUId;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getNameOfOrg() {
		return nameOfOrg;
	}

	public void setNameOfOrg(String nameOfOrg) {
		this.nameOfOrg = nameOfOrg;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}



	public void validate() {
		if(StringUtils.isBlank(getAddress()))
		{
			addFieldError("address", "Address cannot be blank");
		}
		if(StringUtils.isBlank(getNameOfOrg()))
		{
			addFieldError("nameOfOrg", "Name Of Organization cannot be blank");
		}
		if(StringUtils.isBlank(getInterest()))
		{
			addFieldError("interest", "Subject cannot be blank");
		}
		if(StringUtils.isBlank(getArea()))
		{
			addFieldError("area", "Location cannot be blank");
		}
		if(getMinAge()==0 || !StringUtils.isNumeric(String.valueOf(getMinAge())))
		{
			addFieldError("minAge", "Enter valid Min Age");
		}

		if(getMaxAge()==0 || !StringUtils.isNumeric(String.valueOf(getMaxAge())))
		{
			addFieldError("maxAge", "Enter valid Max Age");
		}

		if(getMinAge()>getMaxAge())
		{
			addFieldError("minAge", "Enter valid Min Age");
		}
	}

	@Override
	@SkipValidation
	public String execute() throws Exception {

		System.out.println("Inside execute of NormalUserAction:"+session.get("username"));
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql = "SELECT FirstName,LastName,EmailId,ContactNo,UserId from personalinformation where userName=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, (String)session.get("username"));

			ResultSet rs = ps.executeQuery();

			while(rs.next())
			{
				System.out.println("First name:"+rs.getString(1));
				String sql1="SELECT NameOfOrg,Interest,Area,MinAgeGroup,MaxAgeGroup,Address from normaluserinformation where UserId=?";
				PreparedStatement ps1=connection.prepareStatement(sql1);

				setFirstName(rs.getString(1));
				setLastName(rs.getString(2));
				setEmail(rs.getString(3));
				setPhone(rs.getString(4));
				setUserId(rs.getString(5));
				ps1.setString(1, getUserId());
				ResultSet normalUserInfoSet=ps1.executeQuery();
				while(normalUserInfoSet.next())
				{
					setNameOfOrg(normalUserInfoSet.getString(1));
					setInterest(normalUserInfoSet.getString(2));
					setArea(normalUserInfoSet.getString(3));
					setMinAge(normalUserInfoSet.getInt(4));
					setMaxAge(normalUserInfoSet.getInt(5));
					setAddress(normalUserInfoSet.getString(6));
				}
				ps1.close();
				ret=SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Something Went Wrong!");
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;
	}
	public String saveDetails()
	{
		/*String validationStatus= myValidate();
		if(validationStatus.equals(INPUT))
		{
			return INPUT;
		}*/
		System.out.println("Inside Save Details of NU");
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql=null;
			PreparedStatement preparedStatement=connection.prepareStatement("SELECT NameOfOrg,Interest,Area,MinAgeGroup,MaxAgeGroup,Address from normaluserinformation where UserId=?");
			preparedStatement.setString(1, getUserId());
			ResultSet normalUserInfoSet=preparedStatement.executeQuery();
			while(normalUserInfoSet.next())
			{
				sql="UPDATE normaluserinformation set NameOfOrg=?,Interest=?,Area=?,MinAgeGroup=?,MaxAgeGroup=?,address=? where UserId=?";
				PreparedStatement preparedStatement1=connection.prepareStatement(sql);

				preparedStatement1.setString(1, getNameOfOrg());
				preparedStatement1.setString(2, getInterest());
				preparedStatement1.setString(3, getArea());
				preparedStatement1.setInt(4, getMinAge());
				preparedStatement1.setInt(5, getMaxAge());
				preparedStatement1.setString(6, getAddress());
				preparedStatement1.setString(7, getUserId());
				int rs=preparedStatement1.executeUpdate();
				if(rs==1)
				{
					System.out.println("Entry updated");
					addActionMessage("Details Updated!");
					setAddress("");
					setNameOfOrg("");
					setInterest("");
					setArea("");
					setMinAge(0);
					setMaxAge(0);
					ret=SUCCESS;
				}
			}
			if(sql==null)
			{
				sql = "INSERT INTO normaluserinformation(UserId,NameOfOrg,Interest,Area,MinAgeGroup,MaxAgeGroup,address) "
						+ "VALUES(?,?,?,?,?,?,?)";
				System.out.println("User iD:"+getAddress());
				System.out.println("name of org:"+getNameOfOrg());
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setString(1, getUserId());
				ps.setString(2, getNameOfOrg());
				ps.setString(3, getInterest());
				ps.setString(4, getArea());
				ps.setInt(5, getMinAge());
				ps.setInt(6, getMaxAge());
				ps.setString(7, getAddress());
				int rs = ps.executeUpdate();

				if(rs==1)
				{
					System.out.println("Entry added");
					addActionMessage("Details Saved!");
					setAddress("");
					setNameOfOrg("");
					setInterest("");
					setArea("");
					setMinAge(0);
					setMaxAge(0);
					ret=SUCCESS;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Something Went Wrong!");
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;

	}
	@SkipValidation
	public String fetchRecords()
	{
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql = "SELECT UserId,NameOfOrg,Interest,Area,MinAgeGroup,MaxAgeGroup,Address from normaluserinformation";
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				NormalUserBean bean=new NormalUserBean();
				String sql1="Select FirstName,LastName,EmailId,ContactNo from personalinformation where UserId=?";
				PreparedStatement ps1=connection.prepareStatement(sql1);
				ps1.setString(1, rs.getString(1));
				ResultSet personalInfoSet=ps1.executeQuery();
				while(personalInfoSet.next())
				{
					bean.setFirstName(personalInfoSet.getString(1));
					bean.setLastName(personalInfoSet.getString(2));
					bean.setEmail(personalInfoSet.getString(3));
					bean.setPhone(personalInfoSet.getString(4));

				}
				ps1.close();
				bean.setUserId(rs.getString(1));
				bean.setNameOfOrg(rs.getString(2));
				bean.setInterest(rs.getString(3));
				bean.setArea(rs.getString(4));
				bean.setMinAge(rs.getInt(5));
				bean.setMaxAge(rs.getInt(6));
				getNormalUserList().add(bean);
				ret=SUCCESS;
			}
			if(ret.equals(ERROR))
			{
				addActionError("No data ");
			}
			System.out.println(normalUserList);
			String paramName=ServletActionContext.getRequest().getParameter("nUId");
			System.out.println("Parameter name:"+paramName);
			
			String sql2="Select UserId from tutorinformation where UserId=?";
			PreparedStatement ps2=connection.prepareStatement(sql2);
			ps2.setString(1, getnUId());
			System.out.println("CHeck for:"+getnUId());
			ResultSet rsId=ps2.executeQuery();
			if(!rsId.next())
			{
				flag=1;
			}
			rsId.close();
			ps2.close();
			setnUId(paramName);
			System.out.println("nUId:"+getnUId());
		} catch (Exception e) {
			e.printStackTrace();
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;
	}


	@Override
	public void setSession(Map<String, Object> arg0) {
		this.session=arg0;

	}


}
