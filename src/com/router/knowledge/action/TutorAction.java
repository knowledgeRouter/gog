package com.router.knowledge.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.ActionSupport;
import com.router.knowldege.beans.TutorBean;

public class TutorAction extends ActionSupport implements SessionAware {
	
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String address;
	private String phone;
	private String area;
	private String interest;
	private String userId;
	private String nUId;
	private ArrayList<TutorBean> tutorList;
	private Map<String,Object> session;
	private int flag=0;
	
	
	

		public ArrayList<TutorBean> getTutorList() {
		if(tutorList==null)
		{
			tutorList=new ArrayList<TutorBean>();
		}
		return tutorList;
	}

		
	public int getFlag() {
			return flag;
		}


		public void setFlag(int flag) {
			this.flag = flag;
		}


	public void setTutorList(ArrayList<TutorBean> tutorList) {
		this.tutorList = tutorList;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getnUId() {
		return nUId;
	}

	public void setnUId(String nUId) {
		this.nUId = nUId;
	}
	
	
	public void validate() {
		if(StringUtils.isBlank(getInterest()))
		{
			addFieldError("subject", "Subject cannot be blank");
		}
		if(StringUtils.isBlank(getArea()))
		{
			addFieldError("area", "Location cannot be blank");
		}
	}

	@Override
	@SkipValidation
	public String execute() throws Exception {
		System.out.println("Inside execute of TutorAction");
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql = "SELECT FirstName,LastName,EmailId,ContactNo,UserId from personalinformation where userName=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			System.out.println("Email ID:"+session.get("username"));
			ps.setString(1,(String)session.get("username"));

			ResultSet rs = ps.executeQuery();

			while(rs.next())
			{
				System.out.println("First name:"+rs.getString(1));
				String sql1="SELECT Interest,Area,Address from tutorinformation where UserId=?";
				PreparedStatement ps1=connection.prepareStatement(sql1);
				setFirstName(rs.getString(1));
				setLastName(rs.getString(2));
				setEmail(rs.getString(3));
				setPhone(rs.getString(4));
				setUserId(rs.getString(5));
				ps1.setString(1, getUserId());
				ResultSet tutorInfoSet=ps1.executeQuery();
				while(tutorInfoSet.next())
				{
					setInterest(tutorInfoSet.getString(1));
					setArea(tutorInfoSet.getString(2));
					setAddress(tutorInfoSet.getString(3));
				}
				ps1.close();
				
				ret=SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Something Went Wrong!");
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;

	}
	public String saveDetails()
	{
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql=null;
			PreparedStatement preparedStatement=connection.prepareStatement("SELECT Interest,Area,Address from tutorinformation where UserId=?");
			preparedStatement.setString(1, getUserId());
			ResultSet tutorInfoSet=preparedStatement.executeQuery();
			while(tutorInfoSet.next())
			{
				sql="UPDATE tutorinformation set Interest=?,Area=?,Address=? where UserId=?";
				PreparedStatement preparedStatement1=connection.prepareStatement(sql);
				
				
				preparedStatement1.setString(1, getInterest());
				preparedStatement1.setString(2, getArea());
				preparedStatement1.setString(3, getAddress());
				preparedStatement1.setString(4, getUserId());
				int rs=preparedStatement1.executeUpdate();
				if(rs==1)
				{
					System.out.println("Entry updated");
					addActionMessage("Details Updated!");
					setInterest("");
					setArea("");
					setAddress("");
					ret=SUCCESS;
				}
			}
			if(sql==null){
			sql = "INSERT INTO tutorinformation(UserId,Interest,Area,Address) "
					+ "VALUES(?,?,?,?)";
			System.out.println("User iD:"+getUserId());
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, getUserId());
			ps.setString(2, getInterest());
			ps.setString(3, getArea());
			ps.setString(4, getAddress());
			int rs = ps.executeUpdate();

			if(rs==1)
			{
				System.out.println("Entry added");
				addActionMessage("Details Saved!");
				setInterest("");
				setArea("");
				setAddress("");
				ret=SUCCESS;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Something Went Wrong!");
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;

	}
	
	@SkipValidation
	public String fetchRecords()
	{
		String ret = ERROR;
		Connection connection=null;
		try {
			String URL = "jdbc:mysql://localhost/knowledge_router";
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, "root", "root");
			String sql = "SELECT UserId,Interest,Area from tutorinformation";
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while(rs.next())
			{
				TutorBean bean=new TutorBean();
				String sql1="Select FirstName,LastName,EmailId,ContactNo from personalinformation where UserId=?";
				PreparedStatement ps1=connection.prepareStatement(sql1);
				ps1.setString(1, rs.getString(1));
				ResultSet personalInfoSet=ps1.executeQuery();
				while(personalInfoSet.next())
				{
					bean.setFirstName(personalInfoSet.getString(1));
					bean.setLastName(personalInfoSet.getString(2));
					bean.setEmail(personalInfoSet.getString(3));
					bean.setPhone(personalInfoSet.getString(4));
				}
				ps1.close();
				bean.setUserId(rs.getString(1));
				bean.setInterest(rs.getString(2));
				bean.setArea(rs.getString(3));
				getTutorList().add(bean);
				ret=SUCCESS;
				String paramName=ServletActionContext.getRequest().getParameter("nUId");
				System.out.println("Parameter name:"+paramName);
				setnUId(paramName);
			}
			String sql2="Select UserId from normaluserinformation where UserId=?";
			PreparedStatement ps2=connection.prepareStatement(sql2);
			ps2.setString(1, getnUId());
			System.out.println("CHeck for:"+getnUId());
			ResultSet rsId=ps2.executeQuery();
			if(!rsId.next())
			{
				flag=1;
			}
			rsId.close();
			ps2.close();
			if(ret.equals(ERROR))
			{
				addActionError("No data ");
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Something Went Wrong!");
			ret = ERROR;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
				}
			}
		}
		System.out.println("CHeck for:"+getnUId());
		return ret;
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		this.session=arg0;
		
	}

}
