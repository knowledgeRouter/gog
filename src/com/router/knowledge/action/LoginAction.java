package com.router.knowledge.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport implements SessionAware {
	
	private String userName;
	private String password;
	private int type;
	private Map<String,Object> session;
	
	
	


	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public int getType() {
		return type;
	}



	public void setType(int type) {
		this.type = type;
	}

	@Override
	public void validate() {
		if(StringUtils.isBlank(getUserName()))
		{
			addFieldError("userName", "userName cannot be blank");
		}
		if(StringUtils.isBlank(getPassword()))
		{
			addFieldError("password", "Password cannot be blank");
		}
		if(getType()==-1)
		{
			addFieldError("type", "Please choose type of user");
		}
	}

	public String execute()
	{
		session.put("username", getUserName());
		String ret = ERROR;
		Connection connection=null;
		 try {
	         String URL = "jdbc:mysql://localhost/knowledge_router";
	         Class.forName("com.mysql.jdbc.Driver");
	         connection = DriverManager.getConnection(URL, "root", "root");
	         String sql = "SELECT Password,UserType from personalinformation where userName=?";
	      //   sql+=" user = ? AND password = ?";
	         PreparedStatement ps = connection.prepareStatement(sql);
	       //  ps.setInt(1,1);
	         ps.setString(1, getUserName());
	        
	         String password=null;
	         int userType=-1;
	         ResultSet rs = ps.executeQuery();
	        		 while(rs.next())
	        		 {
	        			  password=rs.getString(1);
	        			  userType=rs.getInt(2);
	        		 }
	        		 if(password.equals(getPassword()))
	        		 {
	        			 if(userType==getType())
	        			 {
	        				 if(getType()==0)
	        					{
	        						return "normalUser";
	        					}
	        					if(getType()==1)
	        					{
	        						return "tutor";
	        					}
	        					if(getType()==2)
	        					{
	        						return "donate";
	        					}
	        			 }
	        			 else
		        		 {
		        			addActionError("Invalid Login"); 
		        		 }
	        		 }
	        		 else
	        		 {
	        			addActionError("Invalid Login"); 
	        		 }
	      } catch (Exception e) {
	    	  e.printStackTrace();
	    	  addActionError("Something Went Wrong");
	         ret = ERROR;
	      } finally {
	         if (connection != null) {
	            try {
	            	connection.close();
	            } catch (Exception e) {
	            }
	         }
	      }
	      return ret;
		
		
	}



	@Override
	public void setSession(Map<String, Object> session) {
		this.session=session;
		
	}
	
	
}
