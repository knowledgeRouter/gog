CREATE TABLE `personalinformation` (
  `UserId` int(10) NOT NULL AUTO_INCREMENT,
  `Password` varchar(20) DEFAULT NULL,
  `EmailId` varchar(40) DEFAULT NULL,
  `FirstName` varchar(30) DEFAULT NULL,
  `LastName` varchar(30) DEFAULT NULL,
  `ContactNo` varchar(15) DEFAULT NULL,
  `UserType` int(11) DEFAULT '-1',
  `userName` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `userName_UNIQUE` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
