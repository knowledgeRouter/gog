CREATE TABLE `tutorinformation` (
  `UserId` int(10) DEFAULT NULL,
  `Interest` varchar(500) DEFAULT NULL,
  `Area` varchar(100) DEFAULT NULL,
  `Address` varchar(200) DEFAULT NULL,
  KEY `UserId` (`UserId`),
  CONSTRAINT `tutorinformation_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `personalinformation` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
