CREATE TABLE `donate` (
  `UserId` int(10) DEFAULT NULL,
  `Item` varchar(30) DEFAULT NULL,
  `DescriptionOfItem` varchar(200) DEFAULT NULL,
  `AddressWhereToPick` varchar(200) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  KEY `donate_ibfk_1` (`UserId`),
  CONSTRAINT `donate_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `personalinformation` (`UserId`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
