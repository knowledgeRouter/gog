CREATE TABLE `normaluserinformation` (
  `UserId` int(10) DEFAULT NULL,
  `NameOfOrg` varchar(30) DEFAULT NULL,
  `Interest` varchar(500) DEFAULT NULL,
  `Area` varchar(100) DEFAULT NULL,
  `MinAgeGroup` int(11) DEFAULT NULL,
  `MaxAgeGroup` int(11) DEFAULT NULL,
  `Address` varchar(200) DEFAULT NULL,
  KEY `normaluserinformation_ibfk_1` (`UserId`),
  CONSTRAINT `normaluserinformation_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `personalinformation` (`UserId`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
